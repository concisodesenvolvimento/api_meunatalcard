﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiMeuNatalCard.Classes
{
    public class AlterarSenha
    {
        public string SenhaAntiga { get; set; }
        public string SenhaNova { get; set; }
    }
}
