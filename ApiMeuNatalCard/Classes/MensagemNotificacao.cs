﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiMeuNatalCard.Classes
{
    public class MensagemNotificacao
    {
        public string Titulo { get; set; }
        public string Mensagem { get; set; }
    }
}
