﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiMeuNatalCard.Classes
{
    public class AlterarEmail
    {
        public string EmailAntigo { get; set; }
        public string EmailNovo { get; set; }
    }
}
