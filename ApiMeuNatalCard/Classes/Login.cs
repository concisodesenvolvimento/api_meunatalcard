﻿namespace ApiMeuNatalCard.Classes
{
    public class Login
    {
        public string Cpf { get; set; }
        public string Senha { get; set; }
        public string Token { get; set; }
    }
}
