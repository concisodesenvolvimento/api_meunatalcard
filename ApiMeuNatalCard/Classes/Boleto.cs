﻿using ApiMeuNatalCard.Classes.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiMeuNatalCard.Classes
{
    public class Boleto
    {
        public static async Task<Tuple<byte[], string>> BoletoAsync(int idBoleto, bool cie)
        {
            byte[] boleto = null;
            string resultado = null;

            if (!cie)
            {
                //boleto = await MetodoHTTP.GetStreamAsync("http://3.83.86.62:2907/boletorecarga/" + idBoleto);
                boleto = await MetodoHTTP.GetStreamAsync("boletorecarga/" + idBoleto);

                //resultado = await MetodoHTTP.GetAsync("http://3.83.86.62:2907/boletorecarga/codigo/" + idBoleto);
                resultado = await MetodoHTTP.GetAsync("boletorecarga/codigo/" + idBoleto);
            }
            else
            {
                //boleto = await MetodoHTTP.GetStreamAsync("http://3.83.86.62:2907/boleto/" + idBoleto);
                boleto = await MetodoHTTP.GetStreamAsync("boleto/" + idBoleto);

                //resultado = await MetodoHTTP.GetAsync("http://3.83.86.62:2907/boleto/codigo/" + idBoleto);
                resultado = await MetodoHTTP.GetAsync("boleto/codigo/" + idBoleto);
            }

            if (boleto != null && resultado != "NotFound" && resultado != "Erro")
            {
                string codigo = JsonConvert.DeserializeObject<string>(resultado);

                return new Tuple<byte[], string>(boleto, codigo);
            }

            return new Tuple<byte[], string>(null, "");
        }
    }
}
