﻿using ApiMeuNatalCard.VindiPost;

namespace ApiMeuNatalCard.Classes
{
    public class DadosCompra
    {
        // Cartão (Recarga e Solicitação)
        public int Servico { get; set; }
        public int FormaPagamento { get; set; }

        public int IdEstudante { get; set; }
        public int IdCartao { get; set; }
        public int IdProduto { get; set; }
        public double Valor { get; set; }
        public PostCartaoVindi Cartao { get; set; }
        
        // Boleto (Recarga e Solicitação)
        public int idEndereco { get; set; }
    }
}
