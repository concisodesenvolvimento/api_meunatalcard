﻿using ApiMeuNatalCard.Model;

namespace ApiMeuNatalCard.Classes
{
    public class RegistroEstudante
    {
        public decimal Saldo { get; set; }
        public Estudante Estudante { get; set; }
    }
}
