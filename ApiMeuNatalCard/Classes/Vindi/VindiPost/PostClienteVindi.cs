﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ApiMeuNatalCard.VindiPost
{
    public class PostClienteVindi
    {
        public class Address
        {
            public string street { get; set; }
            public string number { get; set; }
            public string zipcode { get; set; }
            public string neighborhood { get; set; }
            public string city { get; set; }
            public string state { get; set; }
            public string country { get; set; }
        }

        public class Phone
        {
            public string phone_type { get; set; }
            public string number { get; set; }
        }

        public class Cliente
        {
            public string name { get; set; }
            public string email { get; set; }
            public string code { get; set; }
            public string registry_code { get; set; }
            public Address address { get; set; }
            public Phone phone { get; set; }
        }
    }
}
