﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiMeuNatalCard.VindiPost
{
    public class RespostaClienteVindi
    {
        public class Metadata
        {
        }

        public class Address
        {
            public string street { get; set; }
            public string number { get; set; }
            public string additional_details { get; set; }
            public string zipcode { get; set; }
            public string neighborhood { get; set; }
            public string city { get; set; }
            public string state { get; set; }
            public string country { get; set; }
        }

        public class Phone
        {
            public int id { get; set; }
            public string phone_type { get; set; }
            public string number { get; set; }
            public object extension { get; set; }
        }

        public class Customer
        {
            public int id { get; set; }
            public string name { get; set; }
            public string email { get; set; }
            public string registry_code { get; set; }
            public string code { get; set; }
            public object notes { get; set; }
            public string status { get; set; }
            public DateTime created_at { get; set; }
            public DateTime updated_at { get; set; }
            public Metadata metadata { get; set; }
            public Address address { get; set; }
            public List<Phone> phones { get; set; }
        }

        public class Cliente
        {
            public Customer customer { get; set; }
        }
    }
}
