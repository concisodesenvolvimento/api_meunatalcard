﻿namespace ApiMeuNatalCard.VindiPost
{
    public class PostCartaoVindi
    {
        public string holder_name { get; set; }
        public string registry_code { get; set; }
        public string card_expiration { get; set; }
        public string card_number { get; set; }
        public string card_cvv { get; set; }
        public string payment_method_code { get; set; }
        public string payment_company_code { get; set; }
        public int customer_id { get; set; }
    }
}
