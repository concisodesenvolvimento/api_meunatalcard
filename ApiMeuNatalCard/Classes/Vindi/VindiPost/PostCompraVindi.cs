﻿using System.Collections.Generic;

namespace ApiMeuNatalCard.VindiPost
{
    public class PostCompraVindi
    {
        public class BillItem
        {
            public int product_id { get; set; }
            public double amount { get; set; }
            public string description { get; set; }
        }

        public class PaymentProfile
        {
            public int id { get; set; }
        }

        public class Compra
        {
            public int customer_id { get; set; }
            public int installments { get; set; }
            public string payment_method_code { get; set; }
            public List<BillItem> bill_items { get; set; }
            public PaymentProfile payment_profile { get; set; }
        }
    }
}
