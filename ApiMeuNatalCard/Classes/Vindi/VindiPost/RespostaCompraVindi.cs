﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiMeuNatalCard.VindiPost
{
    public class RespostaCompraVindi
    {
        public class Product
        {
            public int id { get; set; }
            public string name { get; set; }
            public string code { get; set; }
        }

        public class BillItem
        {
            public int id { get; set; }
            public string amount { get; set; }
            public object quantity { get; set; }
            public object pricing_range_id { get; set; }
            public string description { get; set; }
            public object pricing_schema { get; set; }
            public Product product { get; set; }
            public object product_item { get; set; }
            public object discount { get; set; }
        }

        public class GatewayResponseFields
        {
            public string nsu { get; set; }
        }

        public class Gateway
        {
            public int id { get; set; }
            public string connector { get; set; }
        }

        public class PaymentCompany
        {
            public int id { get; set; }
            public string name { get; set; }
            public string code { get; set; }
        }

        public class PaymentProfile
        {
            public int id { get; set; }
            public string holder_name { get; set; }
            public string registry_code { get; set; }
            public object bank_branch { get; set; }
            public object bank_account { get; set; }
            public DateTime card_expiration { get; set; }
            public string card_number_first_six { get; set; }
            public string card_number_last_four { get; set; }
            public string token { get; set; }
            public DateTime created_at { get; set; }
            public PaymentCompany payment_company { get; set; }
        }

        public class LastTransaction
        {
            public int id { get; set; }
            public string transaction_type { get; set; }
            public string status { get; set; }
            public string amount { get; set; }
            public int installments { get; set; }
            public string gateway_message { get; set; }
            public string gateway_response_code { get; set; }
            public string gateway_authorization { get; set; }
            public string gateway_transaction_id { get; set; }
            public GatewayResponseFields gateway_response_fields { get; set; }
            public object fraud_detector_score { get; set; }
            public object fraud_detector_status { get; set; }
            public object fraud_detector_id { get; set; }
            public DateTime created_at { get; set; }
            public Gateway gateway { get; set; }
            public PaymentProfile payment_profile { get; set; }
        }

        public class PaymentMethod
        {
            public int id { get; set; }
            public string public_name { get; set; }
            public string name { get; set; }
            public string code { get; set; }
            public string type { get; set; }
        }

        public class Charge
        {
            public int id { get; set; }
            public string amount { get; set; }
            public string status { get; set; }
            public DateTime due_at { get; set; }
            public object paid_at { get; set; }
            public int installments { get; set; }
            public int attempt_count { get; set; }
            public DateTime next_attempt { get; set; }
            public object print_url { get; set; }
            public DateTime created_at { get; set; }
            public DateTime updated_at { get; set; }
            public LastTransaction last_transaction { get; set; }
            public PaymentMethod payment_method { get; set; }
        }

        public class Customer
        {
            public int id { get; set; }
            public string name { get; set; }
            public string email { get; set; }
            public string code { get; set; }
        }

        public class Metadata
        {
        }

        public class PaymentCompany2
        {
            public int id { get; set; }
            public string name { get; set; }
            public string code { get; set; }
        }

        public class PaymentProfile2
        {
            public int id { get; set; }
            public string holder_name { get; set; }
            public string registry_code { get; set; }
            public object bank_branch { get; set; }
            public object bank_account { get; set; }
            public DateTime card_expiration { get; set; }
            public string card_number_first_six { get; set; }
            public string card_number_last_four { get; set; }
            public string token { get; set; }
            public DateTime created_at { get; set; }
            public PaymentCompany2 payment_company { get; set; }
        }

        public class Bill
        {
            public int id { get; set; }
            public object code { get; set; }
            public string amount { get; set; }
            public int installments { get; set; }
            public string status { get; set; }
            public object seen_at { get; set; }
            public object billing_at { get; set; }
            public DateTime due_at { get; set; }
            public string url { get; set; }
            public DateTime created_at { get; set; }
            public DateTime updated_at { get; set; }
            public List<BillItem> bill_items { get; set; }
            public List<Charge> charges { get; set; }
            public Customer customer { get; set; }
            public object period { get; set; }
            public object subscription { get; set; }
            public Metadata metadata { get; set; }
            public PaymentProfile2 payment_profile { get; set; }
            public object payment_condition { get; set; }
        }

        public class Fatura
        {
            public Bill bill { get; set; }
        }
    }
}
