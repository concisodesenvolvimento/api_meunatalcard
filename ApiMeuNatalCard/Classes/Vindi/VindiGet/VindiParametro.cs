﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ApiMeuNatalCard.VindiGet
{
    public class VindiParametro
    {
        //POST
        public static async System.Threading.Tasks.Task<string> PostAsync(string Json, string url, string sContentType)
        {
            using (HttpClient client = new HttpClient())
            {
                var basicAuth = string.Format("Basic {0}", Convert.ToBase64String(Encoding.UTF8.GetBytes("yxvSMtria41sccNe8rPYVkl8gBEtur1NSQZl_lt_JoA")));
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", basicAuth);

                var uri = new Uri(string.Format(url));
                using (HttpResponseMessage resposta = await client.PostAsync(uri, new StringContent(Json, Encoding.UTF8, sContentType)))
                {
                    try
                    {
                        resposta.EnsureSuccessStatusCode();
                        return await resposta.Content.ReadAsStringAsync();
                    }
                    catch
                    {
                        return "";
                    }
                }
            }
        }

        //GET
        public static async Task<string> GetVindiAsync(string query)
        {
            using (HttpClient client = new HttpClient())
            {
                var basicAuth = string.Format("Basic {0}", Convert.ToBase64String(Encoding.UTF8.GetBytes("yxvSMtria41sccNe8rPYVkl8gBEtur1NSQZl_lt_JoA")));
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", basicAuth);

                var uri = new Uri(string.Format(query));

                using (HttpResponseMessage resposta = await client.GetAsync(uri))
                {
                    try
                    {
                        resposta.EnsureSuccessStatusCode();

                        string result = await resposta.Content.ReadAsStringAsync();

                        return result;
                    }
                    catch
                    {
                        return null;
                    }
                }
            }
        }
    }
}
