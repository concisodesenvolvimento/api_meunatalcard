﻿namespace ApiMeuNatalCard.VindiGet
{
    public class DadosCartao
    {
        public string Bandeira { get; set; }
        public string Metodo { get; set; }
        public int UltimosNumero { get; set; }
        public int Idcartao { get; set; }
    }
}
