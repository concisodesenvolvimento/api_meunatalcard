﻿using System;
using System.Collections.Generic;

namespace ApiMeuNatalCard.VindiGet
{
    class GetCartaoVindi
    {
        public class PaymentCompany
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Code { get; set; }
        }

        public class PaymentMethod
        {
            public int Id { get; set; }
            public string Public_name { get; set; }
            public string Name { get; set; }
            public string Code { get; set; }
            public string Type { get; set; }
        }

        public class Customer
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Email { get; set; }
            public object Code { get; set; }
        }

        public class PaymentProfile
        {
            public int Id { get; set; }
            public string Status { get; set; }
            public string Holder_name { get; set; }
            public string Registry_code { get; set; }
            public object Bank_branch { get; set; }
            public object Bank_account { get; set; }
            public DateTime Card_expiration { get; set; }
            public string Card_number_first_six { get; set; }
            public string Card_number_last_four { get; set; }
            public string Token { get; set; }
            public string Gateway_token { get; set; }
            public string Type { get; set; }
            public DateTime Created_at { get; set; }
            public DateTime Updated_at { get; set; }
            public PaymentCompany Payment_company { get; set; }
            public PaymentMethod Payment_method { get; set; }
            public Customer Customer { get; set; }
        }

        public class Cartao
        {
            public List<PaymentProfile> Payment_profiles { get; set; }
        }
    }
}
