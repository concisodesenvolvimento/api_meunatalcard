﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ApiMeuNatalCard.Classes.Http
{
    public class MetodoHTTP
    {
        private static string enderecoBase = "https://meunatalcard-hm.concisoti.app:2907/";/*"http://18.215.93.172:2907/";*/
        ////POST
        public static async Task<string> PostAsync(string Json, string url, string sContentType)
        {
            using (HttpClient client = new HttpClient())
            {
                var basicAuth = string.Format("Basic {0}", Convert.ToBase64String(Encoding.UTF8.GetBytes("23456789")));
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", basicAuth);

                client.BaseAddress = new Uri(enderecoBase);
                
                using (HttpResponseMessage resposta = await client.PostAsync(url, new StringContent(Json, Encoding.UTF8, sContentType)))
                {
                    try
                    {
                        resposta.EnsureSuccessStatusCode();
                        return await resposta.Content.ReadAsStringAsync();
                    }
                    catch
                    {
                        if (resposta.StatusCode == System.Net.HttpStatusCode.NotFound)
                        {
                            return "NotFound";
                        }
                        else
                        {
                            return "Erro";
                        }
                    }
                }
            }
        }

        //GET
        public static async Task<string> GetAsync(string url)
        {
            using (HttpClient client = new HttpClient())
            {
                var basicAuth = string.Format("Basic {0}", Convert.ToBase64String(Encoding.UTF8.GetBytes("23456789")));
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", basicAuth);
                
                client.BaseAddress = new Uri(enderecoBase);

                using (HttpResponseMessage resposta = await client.GetAsync(url))
                {
                    try
                    {
                        resposta.EnsureSuccessStatusCode();

                        return await resposta.Content.ReadAsStringAsync();
                    }
                    catch
                    {
                        if (resposta.StatusCode == System.Net.HttpStatusCode.NotFound)
                        {
                            return "NotFound";
                        }
                        else
                        {
                            return "Erro";
                        }
                    }
                }
            }
        }

        //GET
        //public static async System.Threading.Tasks.Task<string> GetAsync(string url, string BODY, string sContentType)
        //{
        //    if (await NetWorkdisponibilidadeAsync())
        //    {
        //        using (HttpClient client = new HttpClient())
        //        {
        //            var uri = new Uri(string.Format(url));
        //            string Json = JsonConvert.SerializeObject(BODY);

        //            using (HttpResponseMessage resposta = await client.PostAsync(uri, new StringContent(Json, Encoding.UTF8, sContentType)))
        //            {
        //                try
        //                {
        //                    resposta.EnsureSuccessStatusCode();
        //                    return await resposta.Content.ReadAsStringAsync();
        //                }
        //                catch
        //                {
        //                    if (resposta.StatusCode == System.Net.HttpStatusCode.NotFound)
        //                    {
        //                        return "NotFound";
        //                    }
        //                    else
        //                    {
        //                        return "Erro";
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    else
        //        return "NoConected";
        //}

        //GET
        public static async System.Threading.Tasks.Task<byte[]> GetStreamAsync(string url)
        {
            using (HttpClient client = new HttpClient())
            {
                var basicAuth = string.Format("Basic {0}", Convert.ToBase64String(Encoding.UTF8.GetBytes("23456789")));
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", basicAuth);
                
                client.BaseAddress = new Uri(enderecoBase);
                
                using (HttpResponseMessage resposta = await client.GetAsync(url))
                {
                    try
                    {
                        resposta.EnsureSuccessStatusCode();

                        return await resposta.Content.ReadAsByteArrayAsync();
                    }
                    catch
                    {
                        return null;
                    }
                }
            }
        }

        ////PUT
        //public static async System.Threading.Tasks.Task<bool> PutAsync(string Json, string url, string sContentType)
        //{
        //    if (await NetWorkdisponibilidadeAsync())
        //    {
        //        using (HttpClient client = new HttpClient())
        //        {
        //            var basicAuth = string.Format("Basic {0}", Convert.ToBase64String(Encoding.UTF8.GetBytes("23456789")));
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //            client.DefaultRequestHeaders.Add("Authorization", basicAuth);
        //            var uri = new Uri(string.Format(url));
        //            using (HttpResponseMessage resposta = await client.PutAsync(uri, new StringContent(Json, Encoding.UTF8, sContentType)))
        //            {
        //                try
        //                {
        //                    resposta.EnsureSuccessStatusCode();
        //                    return true;
        //                }
        //                catch
        //                {
        //                    return false;
        //                }
        //            }
        //        }
        //    }
        //    else
        //        return false;
        //}

        ////PUT
        //public static async System.Threading.Tasks.Task<bool> PutAsync(string url)
        //{
        //    if (await NetWorkdisponibilidadeAsync())
        //    {
        //        using (HttpClient client = new HttpClient())
        //        {
        //            var basicAuth = string.Format("Basic {0}", Convert.ToBase64String(Encoding.UTF8.GetBytes("23456789")));
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //            client.DefaultRequestHeaders.Add("Authorization", basicAuth);
        //            var uri = new Uri(string.Format(url));
        //            using (HttpResponseMessage resposta = await client.PutAsync(uri, new StringContent("application/json")))
        //            {
        //                try
        //                {
        //                    resposta.EnsureSuccessStatusCode();

        //                    //client.Dispose();
        //                    //resposta.Dispose();

        //                    return true;
        //                }
        //                catch
        //                {
        //                    return false;
        //                }
        //            }
        //        }
        //    }
        //    else
        //        return false;
        //}

        ////DELETE
        //public static async System.Threading.Tasks.Task<bool> DeleteAsync(string url)
        //{
        //    if (await NetWorkdisponibilidadeAsync())
        //    {
        //        using (HttpClient client = new HttpClient())
        //        {
        //            var uri = new Uri(string.Format(url));

        //            using (HttpResponseMessage resposta = await client.DeleteAsync(uri))
        //            {
        //                try
        //                {
        //                    resposta.EnsureSuccessStatusCode();

        //                    return true;
        //                }
        //                catch
        //                {
        //                    return false;
        //                }
        //            }
        //        }
        //    }
        //    else
        //        return false;
        //}
    }
}
