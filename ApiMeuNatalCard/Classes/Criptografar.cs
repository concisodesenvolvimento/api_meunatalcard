﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ApiMeuNatalCard.Classes
{
    public class Criptografar
    {
        public static String Sha256_hash(string valor)
        {
            using (SHA256 hash = SHA256Managed.Create())
            {
                return String.Concat(hash
                  .ComputeHash(Encoding.UTF8.GetBytes(valor))
                  .Select(item => item.ToString("x2"))).ToUpper();
            }
        }
    }
}
