﻿using System;

namespace ApiMeuNatalCard.Classes
{
    public class DadosHistorico
    {
        public int IdRecargaSolicitacao { get; set; }
        public DateTime DataTransacao { get; set; }
        public double Valor { get; set; }
        public string NomeFantasia { get; set; }
        public int TipoRecarga { get; set; }
        public int? Status { get; set; }

        public int? IdBoleto { get; set; }
    }

    public enum RecargaStatus
    {
        AguardandoPagamento = 1,
        Pago = 2,
        Cancelado = 9,
        CreditoLiberado = 10,
        Finalizado = 11,
        CreditoNaoLiberado = 12,
        Nulo = -1
    }
}
