﻿using System;

namespace ApiMeuNatalCard.Classes.ApiIee
{
    public class DadosApiIee
    {
        public class ApiIee
        {
            public int IdEstudante { get; set; }
            public string Cpf { get; set; }
            public string Rg { get; set; }
            public string Matricula { get; set; }
            public string Nome { get; set; }
            public DateTime DataNascimento { get; set; }
            public string Endereco { get; set; }
            public string Numero { get; set; }
            public string Cep { get; set; }
            public string Bairro { get; set; }
            public string Municipio { get; set; }
            public string Celular { get; set; }
            public string Telefone { get; set; }
            public string Email { get; set; }
            public string Status { get; set; }
            public string Instituicao { get; set; }
            public int AnoEmissao { get; set; }
            public byte[] Foto { get; set; }
            public string NomeCurso { get; set; }
            public string Nivel { get; set; }
            public string CardId { get; set; }
            public string StatusCartao { get; set; }
            public decimal Saldo { get; set; }
            public int BloqueioBiometrico { get; set; }
            public string CodigoNacional { get; set; }
            public byte[] QrCode { get; set; }
        }
    }
}
