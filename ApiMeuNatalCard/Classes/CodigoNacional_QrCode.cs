﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiMeuNatalCard.Classes
{
    public class CodigoNacional_QrCode
    {
        public string CodigoNacional { get; set; }
        public byte[] QrCode { get; set; }
    }
}
