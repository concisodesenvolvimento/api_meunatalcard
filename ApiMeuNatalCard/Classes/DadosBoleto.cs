﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiMeuNatalCard.Classes
{
    public class DadosBoleto
    {
        public int IdSolicitacao { get; set; }
        public int IdBoleto { get; set; }
    }
}
