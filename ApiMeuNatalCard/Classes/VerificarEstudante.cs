﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiMeuNatalCard.Classes
{
    public class VerificarEstudante
    {
        public string Cpf { get; set; }
        public DateTime DataNascimento { get; set; }
    }
}
