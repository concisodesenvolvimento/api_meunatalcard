﻿using Microsoft.EntityFrameworkCore;
using ApiMeuNatalCard.Model;

namespace ApiMeuNatalCard
{
    public class ApiContext : DbContext
    {
        public ApiContext (DbContextOptions<ApiContext> options)
            : base(options)
        {
        }

        public DbSet<ApiMeuNatalCard.Model.Estudante> Estudantes { get; set; }
        public DbSet<ApiMeuNatalCard.Model.Endereco> Enderecos { get; set; }
        public DbSet<ApiMeuNatalCard.Model.Vindi> Vindi { get; set; }
        public DbSet<ApiMeuNatalCard.Model.CompraAplicativo> CompraAplicativo { get; set; }
        public DbSet<ApiMeuNatalCard.Model.Produto> Produto { get; set; }
        public DbSet<ApiMeuNatalCard.Model.AcessoAplicativo> AcessoAplicativo { get; set; }
        public DbSet<ApiMeuNatalCard.Model.NotificacaoEstudante> NotificacaoEstudante { get; set; }
        public DbSet<ApiMeuNatalCard.Model.ChavesAcesso> ChavesAcesso { get; set; }
        public DbSet<ApiMeuNatalCard.Model.Saldos> Saldos { get; set; }
        public DbSet<ApiMeuNatalCard.Model.ConfigEstudante> ConfigEstudante { get; set; }
        public DbSet<ApiMeuNatalCard.Model.CadastroAplicativo> CadastroAplicativo { get; set; }
        
    }
}
