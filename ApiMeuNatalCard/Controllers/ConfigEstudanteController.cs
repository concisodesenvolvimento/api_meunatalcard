﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ApiMeuNatalCard;
using ApiMeuNatalCard.Model;

namespace ApiMeuNatalCard.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConfigEstudanteController : ControllerBase
    {
        private readonly ApiContext _context;

        public ConfigEstudanteController(ApiContext context)
        {
            _context = context;
        }

        // GET: api/ConfigEstudante
        //[HttpGet]
        //public IEnumerable<ConfigEstudante> GetConfigEstudante()
        //{
        //    return _context.ConfigEstudante;
        //}

        //// GET: api/ConfigEstudante/5
        //[HttpGet("{id}")]
        //public async Task<IActionResult> GetConfigEstudante([FromRoute] int id)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var configEstudante = await _context.ConfigEstudante.FindAsync(id);

        //    if (configEstudante == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(configEstudante);
        //}

        // PUT: api/ConfigEstudante/5

        [HttpPut/*("{id}")*/]
        public async Task<IActionResult> PutConfigEstudante([FromBody] ConfigEstudante configEstudante)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            _context.Entry(configEstudante).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return BadRequest("Erro");
            }

            return NoContent();
        }

        // POST: api/ConfigEstudante
        //[HttpPost]
        //public async Task<IActionResult> PostConfigEstudante([FromBody] ConfigEstudante configEstudante)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    _context.ConfigEstudante.Add(configEstudante);
        //    await _context.SaveChangesAsync();

        //    return CreatedAtAction("GetConfigEstudante", new { id = configEstudante.IdEstudante }, configEstudante);
        //}

        //// DELETE: api/ConfigEstudante/5
        //[HttpDelete("{id}")]
        //public async Task<IActionResult> DeleteConfigEstudante([FromRoute] int id)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var configEstudante = await _context.ConfigEstudante.FindAsync(id);
        //    if (configEstudante == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.ConfigEstudante.Remove(configEstudante);
        //    await _context.SaveChangesAsync();

        //    return Ok(configEstudante);
        //}

        //private bool ConfigEstudanteExists(int id)
        //{
        //    return _context.ConfigEstudante.Any(e => e.IdEstudante == id);
        //}
    }
}