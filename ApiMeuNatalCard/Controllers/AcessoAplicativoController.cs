﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ApiMeuNatalCard.Model;
using System.Security.Cryptography;
using System.Text;
using ApiMeuNatalCard.Classes.Http;
using ApiMeuNatalCard.Classes.ApiIee;
using Newtonsoft.Json;
using ApiMeuNatalCard.Classes;

namespace ApiMeuNatalCard.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AcessoAplicativoController : ControllerBase
    {
        private readonly ApiContext _context;

        public AcessoAplicativoController(ApiContext context)
        {
            _context = context;
        }

        // PUT: api/AcessoAplicativo
        [HttpPut]
        public async Task<IActionResult> PutAcessoAplicativo([FromBody] AcessoAplicativo acessoAplicativo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var acesso = _context.AcessoAplicativo.FirstOrDefault(x => x.IdEstudante == acessoAplicativo.IdEstudante && x.Token == acessoAplicativo.Token);

            if (acesso == null)
            {
                return BadRequest();
            }

            acesso.Ativo = 0;
            acesso.Data_Logof = DateTime.Now;
            _context.Entry(acesso).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AcessoAplicativoExists(acessoAplicativo.IdEstudante))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok();
        }

        // POST: api/AcessoAplicativo
        [HttpPost]
        public async Task<IActionResult> PostAcessoAplicativo([FromBody] Login login)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // ======= VERIFICANDO O LOGIN E SENHA ====== //
            login.Senha = Criptografar.Sha256_hash(login.Senha);

            Estudante estudante = _context.Estudantes.FirstOrDefault(x => x.Cpf == login.Cpf && x.SenhaHash == login.Senha);

            if (estudante == null)
            {
                return NotFound();
            }

            // ======= INCLUIR A LISTA DE ENDEREÇOS DO ESTUDANTE
            estudante.Endereco = _context.Enderecos.FirstOrDefault(e => e.IdEstudante == estudante.IdEstudante && e.Ativo == 1);

            // ======= INCLUIR AS 10 ÚLTIMAS NOTIFICAÇÕES
            estudante.NotificacaoEstudante = _context.NotificacaoEstudante.Where(ne => ne.IdEstudante == estudante.IdEstudante)
                .OrderByDescending(o => o.IdNotificacao).Take(10).ToList();

            // -------------------------------------------------------------------------------------------------------------------- //
            // ======= BUSCA OS DADOS DO BD IEE
            string dadosApiIee = await MetodoHTTP.GetAsync("estudante/" + estudante.Cpf);
            string qrCode = await MetodoHTTP.GetAsync("estudante/qrcode/" + estudante.Cpf);

            if (dadosApiIee == "Erro" || dadosApiIee == "NotFound" || qrCode == "Erro" || qrCode == "NotFound")
            {
                return BadRequest("IeeQrCode");
            }

            // ======= retorno para a APi os dados Foto, Ano emissão e Status do estudante
            DadosApiIee.ApiIee apiIee = JsonConvert.DeserializeObject<DadosApiIee.ApiIee>(dadosApiIee);
            CodigoNacional_QrCode codigo = JsonConvert.DeserializeObject<CodigoNacional_QrCode>(qrCode);

            if (apiIee.Status != "R" || (apiIee.AnoEmissao <= (DateTime.Now.Year - 1) && DateTime.Now.Month > 3))
                apiIee.Status = "I";

            // -------------------------------------------------------------------------------------------------------------------- //
            // ======= VERIFICA SE O ESTUDANTE TEM ALGUMA SOLICITAÇÃO CIE
            int idSolicitacao;

            try
            {
                idSolicitacao = _context.CompraAplicativo.FirstOrDefault(x => x.IdEstudante == estudante.IdEstudante &&
                                                                                         x.Servico == 2 &&
                                                                                         (x.StatusCompra != 0 &&
                                                                                          x.StatusCompra != 9 &&
                                                                                          x.StatusCompra != 11)).IdSolicitacao;
            }
            catch
            {
                idSolicitacao = -1;
            }
            

            //int statusSolicitacao = compraAplicativo != null ? compraAplicativo.StatusCompra : -1;
            //int idEnderecoSolicitacao = compraAplicativo != null ? (int)compraAplicativo.IdEndereco : -1;
            //int idFatura = compraAplicativo != null ? compraAplicativo.IdFatura : -1;
            //int formaPagamentoSolicitacao = compraAplicativo != null ? compraAplicativo.IdFormaPagamento : -1;

            //string mensagemNotificacao = "";
            //int reenviar = 0;

            //if(statusSolicitacao == 8)
            //{
            //    mensagemNotificacao = _context.NotificacaoEstudante.LastOrDefault(n => n.IdEstudante == estudante.IdEstudante).Mensagem;

            //    string[] palavras = mensagemNotificacao.Split(' ');

            //    if ((palavras.Contains("FOTO") || palavras.Contains("foto")) && 
            //        (palavras.Contains("escola") || palavras.Contains("ESCOLA") ))
            //        reenviar = 3;
            //    else if (palavras.Contains("FOTO") || palavras.Contains("foto"))
            //        reenviar = 1;
            //    else
            //        reenviar = 2;
            //}

            // -------------------------------------------------------------------------------------------------------------------- //
            // ======= SALDO DO ESTUDANTE ATIVO
            Saldos saldos = _context.Saldos.FirstOrDefault(x => x.IdEstudante == estudante.IdEstudante);

            decimal saldo = saldos.Saldo;

            string dataSaldo = string.Format("{0:00}/{1:00}/{2:0000} às {3:00}:{4:00}h", saldos.DataAtualizacao.Day, saldos.DataAtualizacao.Month, saldos.DataAtualizacao.Year, saldos.DataAtualizacao.Hour, saldos.DataAtualizacao.Minute);
            
            // -------------------------------------------------------------------------------------------------------------------- //
            // ======= BUSCANDO A CONFIGURACAO DE NOTIFICACAO
            int idConfig = _context.ConfigEstudante.First(x => x.IdEstudante == estudante.IdEstudante).IdConfig;

            // -------------------------------------------------------------------------------------------------------------------- //
            // ======= ARMAZENANDO O TOKEN AO ACESSAR O APP ====== //
            var acesso = _context.AcessoAplicativo.FirstOrDefault(x => x.IdEstudante == estudante.IdEstudante && x.Token == login.Token);

            if (acesso == null)
            {
                AcessoAplicativo acessoAplicativo = new AcessoAplicativo
                {
                    Ativo = 1,
                    Data_Logon = DateTime.Now,
                    IdEstudante = estudante.IdEstudante,
                    Token = login.Token,
                    Falha = 0
                };

                _context.AcessoAplicativo.Add(acessoAplicativo);
            }
            else
            {
                acesso.Data_Logon = DateTime.Now;
                acesso.Ativo = 1;
                _context.Entry(acesso).State = EntityState.Modified;
            }

            await _context.SaveChangesAsync();


            string StatusCartao = (apiIee.StatusCartao == "BLOQUEADO" || apiIee.BloqueioBiometrico == 1) ? "BLOQUEADO" : "ATIVO";
            // ======= DADOS ENVIADOS PARA O APP
            var dados = new
            {
                estudante,
                idConfig,
                apiIee.Status,
                apiIee.AnoEmissao,
                apiIee.Foto,
                apiIee.CardId,
                StatusCartao,
                idSolicitacao,
                saldo,
                dataSaldo,
                codigo.CodigoNacional,
                codigo.QrCode
            };

            return Ok(dados);
        }

        private bool AcessoAplicativoExists(int id)
        {
            return _context.AcessoAplicativo.Any(e => e.IdEstudante == id);
        }
    }
}