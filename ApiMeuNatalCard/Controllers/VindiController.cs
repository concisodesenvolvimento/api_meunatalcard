﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ApiMeuNatalCard.Model;
using ApiMeuNatalCard.VindiGet;
using Newtonsoft.Json;
using ApiMeuNatalCard.Classes;

namespace ApiMeuNatalCard.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VindiController : ControllerBase
    {
        private readonly ApiContext _context;

        public VindiController(ApiContext context)
        {
            _context = context;
        }

        // ======= MÉTODO GET: CLASSE QUE VERIFICA SE O ESTUDANTE TEM CARTÃO CADASTRADO E RETORNA O ID, BANDEIRA E MÉTODO.
        [HttpGet("{idEstudante}")]
        public async Task<IActionResult> GetVindi([FromRoute] int idEstudante)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var estudanteVindi = _context.Vindi.Where(x => x.IdEstudante == idEstudante).ToList();

            if (estudanteVindi.Count != 0)
            {
                string dados = await VindiParametro.GetVindiAsync("https://app.vindi.com.br/api/v1/payment_profiles?query=(customer_id:" + estudanteVindi[0].IdVindi + " and status=active)");

                GetCartaoVindi.Cartao cartao = JsonConvert.DeserializeObject<GetCartaoVindi.Cartao>(dados);

                if (cartao.Payment_profiles.Count != 0)
                {
                    List<DadosCartao> dadosCartaos = new List<DadosCartao>();

                    foreach (GetCartaoVindi.PaymentProfile card in cartao.Payment_profiles)
                    {
                        DadosCartao dadosCartao = new DadosCartao
                        {
                            Bandeira = card.Payment_company.Name.ToString(),
                            Metodo = card.Payment_method.Code.ToString(),
                            UltimosNumero = Convert.ToInt32(card.Card_number_last_four),
                            Idcartao = card.Id
                        };

                        dadosCartaos.Add(dadosCartao);
                    }

                    return Ok(dadosCartaos);
                }
                else
                {
                    return NotFound();
                }
            }
            else
                return NotFound();
        }

        // ======= MÉTODO POST: CLASSE QUE CADASTRA O ESTUDANTE NA VINDI, CADASTRA CARTÃO E REALIZA COMPRA
        // POST: api/Vindi
        [HttpPost]
        public IActionResult PostVindi([FromBody] DadosCompra dadosCompra)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Estudante estudante = _context.Estudantes.FirstOrDefault(x => x.IdEstudante == dadosCompra.IdEstudante);

            // ======= VERIFICA SE O ID DO ESTUDANTE EXISTE NA TABELA ESTUDANTE
            //if (estudante != null)
            //{
            //    CompraAplicativo compraAplicativo;

            //    if (dadosCompra.Servico == 1)
            //    {
            //        if (dadosCompra.FormaPagamento == 16)
            //        {
            //            var CompraNoCartao = CompraNoCartaoAsync(dadosCompra, estudante.Cpf);

            //            if (CompraNoCartao.Result.Item2 != "paid" && CompraNoCartao.Result.Item2 != "pending")
            //                return BadRequest("NãoAutorizado");

            //            compraAplicativo = CompraNoCartao.Result.Item1;

            //            _context.CompraAplicativo.Add(compraAplicativo);

            //            await _context.SaveChangesAsync();

            //            return Ok(CompraNoCartao.Result.Item2);
            //        }
            //        else
            //        {
            //            var resultado = await CompraNoBoletoAsync(dadosCompra, estudante.IdEstudanteIee, estudante.Cpf);

            //            if (resultado.Item1 == null)
            //                return BadRequest("NãoAutorizado");

            //            compraAplicativo = CompraAPP(dadosCompra.IdEstudante, resultado.Item1.IdBoleto, 1, DateTime.Now, resultado.Item2, "Esperando Pagamento", dadosCompra.Servico);

            //            _context.CompraAplicativo.Add(compraAplicativo);

            //            await _context.SaveChangesAsync();

            //            return Ok(resultado.Item1.IdBoleto.ToString());
            //        }
            //    }
            //    else
            //    {
            //        if (dadosCompra.FormaPagamento == 16)
            //        {
            //            var CompraNoCartao = CompraNoCartaoAsync(dadosCompra, estudante.Cpf);

            //            if (CompraNoCartao.Result.Item2 != "paid" && CompraNoCartao.Result.Item2 != "pending")
            //                return NotFound("Transação não realizada");

            //            compraAplicativo = CompraNoCartao.Result.Item1;

            //            _context.CompraAplicativo.Add(compraAplicativo);

            //            await _context.SaveChangesAsync();

            //            return Ok(CompraNoCartao.Result.Item2);
            //        }
            //        else
            //        {
            //            var resultado = await CompraNoBoletoAsync(dadosCompra, estudante.IdEstudanteIee, estudante.Cpf);

            //            if (resultado.Item1 == null)
            //                return BadRequest("NãoAutorizado");

            //            compraAplicativo = CompraAPP(dadosCompra.IdEstudante, resultado.Item1.IdBoleto, 1, DateTime.Now, resultado.Item2, "Esperando Pagamento", dadosCompra.Servico);

            //            _context.CompraAplicativo.Add(compraAplicativo);

            //            await _context.SaveChangesAsync();

            //            return Ok(resultado.Item1.IdBoleto.ToString());
            //        }
            //    }
            //}
            //else
            return NotFound();
        }

        // -----------------------------------------------------------------------------------------------------------------------------
        // -----------------------------------------------------------------------------------------------------------------------------
        // ======= COMPRA NO CARTÃO
        //private async Task<Tuple<CompraAplicativo, string>> CompraNoCartaoAsync(DadosCompra dadosCompra, string cpf)
        //{
        //    // ======= VERIFICA SE EXISTE UM USUÁRIO CADASTRADO NA TABELA ESTUDANTEVINDI
        //    Vindi dados = _context.Vindi.FirstOrDefault(x => x.IdEstudante == dadosCompra.IdEstudante);

        //    // ============================================================================= //
        //    // ======================== CADASTRO DE USUÁRIO NA VINDI ======================= //

        //    int idVindiEstudante;
        //    int idCartaoEstudante;

        //    // ======= CASO A VARIÁVEL DADOS NÃO CONTENHA NENHUM DADOS, ENTÃO É REALIZADO O CADASTRO DO USUÁRIO
        //    if (dados == null)
        //    {
        //        string resultCliente = await CadastrarClienteVindiAsync(dadosCompra.IdEstudante);

        //        RespostaClienteVindi.Cliente cliente = JsonConvert.DeserializeObject<RespostaClienteVindi.Cliente>(resultCliente);

        //        idVindiEstudante = cliente.customer.id;

        //        Vindi vindi = new Vindi
        //        {
        //            IdEstudante = dadosCompra.IdEstudante,
        //            IdVindi = idVindiEstudante
        //        };

        //        _context.Vindi.Add(vindi);
        //        await _context.SaveChangesAsync();
        //    }
        //    else
        //        idVindiEstudante = dados.IdVindi;

        //    // ============================================================================= //
        //    // ======================== CADASTRO DE CARTÃO NA VINDI ======================== //

        //    // ======= SE O USUÁRIO ENVIAR OS DADOS DO CARTÃO, ENTÃO É REALIZADO O CADASTRO DO CARTÃO
        //    if (dadosCompra.Cartao != null)
        //    {
        //        string resultCartao = await CadastrarCartaoVindiAsync(idVindiEstudante, dadosCompra.Cartao);

        //        RespostaCartaoVindi.Cartao cartao = JsonConvert.DeserializeObject<RespostaCartaoVindi.Cartao>(resultCartao);

        //        idCartaoEstudante = cartao.payment_profile.id;
        //    }
        //    // ======= SE O USUÁRIO ENVIAR O ID DO CARTÃO, ENTÃO É ARMAZENADO NA VARIÁVEL ID CARTÃO
        //    else
        //        idCartaoEstudante = dadosCompra.IdCartao;
        //    // ============================================================================= //

        //    // ============================================================================= //
        //    // ======= BUSCAR O PRODUTO CADASTRADO NA VINDI NO BANCO DE DADOS DO APP ======= //
        //    var produto = _context.Produto.First(x => x.IdProduto == dadosCompra.IdProduto);

        //    int idProduto = produto.IdProdutoVindi;

        //    string descricao = produto.Descricao;
        //    // ============================================================================= //

        //    double Valor = dadosCompra.IdProduto == 8 ? 25.00f : dadosCompra.Valor;

        //    // ======= ENVIA OS DADOS PARA REALIZAR A COMPRA (IDVINDI, IDCARTAO, IDPRODUTO, crédito, VALOR, DESCRIÇÃO)
        //    string resultCompra = await ComprarAsync(idVindiEstudante, idCartaoEstudante, idProduto, "credit_card", Valor, descricao);

        //    if (resultCompra == "")
        //        return new Tuple<CompraAplicativo, string>(null, "");

        //    RespostaCompraVindi.Fatura fatura = JsonConvert.DeserializeObject<RespostaCompraVindi.Fatura>(resultCompra);

        //    // ======= STATUS DA COMPRA
        //    if (fatura.bill.status == "pending" || fatura.bill.status == "paid")
        //    {
        //        if (dadosCompra.IdProduto == 8)
        //            await EnviarSolicitacaoAsync(dadosCompra.IdEstudante, dadosCompra.IdLocal, 16, cpf, idCartaoEstudante);

        //        // ======= ESTRUTURA PARA ARMAZENAR A COMPRA REALIZADA NO BD COMPRAAPLICATIVO
        //        CompraAplicativo compraAplicativo = CompraAPP(dadosCompra.IdEstudante, fatura.bill.id, 16, fatura.bill.created_at, fatura.bill.amount, fatura.bill.status, dadosCompra.Servico);

        //        return new Tuple<CompraAplicativo, string>(compraAplicativo, fatura.bill.status);
        //    }

        //    return new Tuple<CompraAplicativo, string>(null, "");
        //}

        //// ======= COMPRA NO BOLETO
        //private async Task<Tuple<DadosBoleto, string>> CompraNoBoletoAsync(DadosCompra dadosCompra, int idIee, string cpf)
        //{
        //    string valor = dadosCompra.IdProduto == 8 ? "25.00" : dadosCompra.Valor.ToString("###.#0");

        //    if (dadosCompra.IdProduto == 8)
        //    {
        //        int local = dadosCompra.IdLocal == 0 ? 1 : 2;

        //        DadosBoleto dadosBoleto = await EnviarSolicitacaoAsync(dadosCompra.IdEstudante, dadosCompra.IdLocal, 1, cpf, null);

        //        return new Tuple<DadosBoleto, string>(dadosBoleto, "25.00");
        //    }
        //    else
        //    {
        //        var body = new
        //        {
        //            idestudante = idIee,
        //            cpf,
        //            valor,
        //            Tipocobranca = 1
        //        };

        //        string url = "http://10.100.110.5:2907/recarga";

        //        string Json = JsonConvert.SerializeObject(body);

        //        string resultado = await MetodoHTTP.PostAsync(Json, url, "application/json");

        //        if (resultado == "NotFound" || resultado == "Erro")
        //            return new Tuple<DadosBoleto, string>(null, "");

        //        DadosBoleto dadosBoleto = JsonConvert.DeserializeObject<DadosBoleto>(resultado);

        //        return new Tuple<DadosBoleto, string>(dadosBoleto, valor);
        //    }
        //}

        //// ======= ESTRUTURA DA COMPRA PARA ARMAZENAR NO BANCO COMPRAAPLICATIVO
        //private CompraAplicativo CompraAPP(int idEstudante, int idCompra, int idFormaPagamento, DateTime dataCompra, string valorCompra, string statusCompra, string servico)
        //{
        //    CompraAplicativo compra = new CompraAplicativo
        //    {
        //        //IdEstudante = idEstudante,
        //        //IdCompra = idCompra,
        //        //IdFormaPagamento = idFormaPagamento,
        //        //DataCompra = dataCompra,
        //        //ValorCompra = valorCompra,
        //        //StatusCompra = statusCompra,
        //        //Servico = servico
        //    };

        //    return compra;
        //}

        //// ======= ENVIANDO DADOS PARA O BD SOLICITAÇÃO DE CARTEIRA
        //private async Task<DadosBoleto> EnviarSolicitacaoAsync(int idEstudante, int idLocal, int tipoCobranca, string cpf, int? idCartao)
        //{
        //    int local = idLocal != 0 ? 2 : 1;

        //    int status = tipoCobranca == 1 ? 1 : 2;

        //    var body = new
        //    {
        //        cpf,
        //        local,
        //        tipoCobranca
        //    };

        //    string url = "http://10.100.110.5:2907/solicitacao";

        //    string Json = JsonConvert.SerializeObject(body);

        //    string resultado = await MetodoHTTP.PostAsync(Json, url, "application/json");

        //    if (resultado != "NotFound" && resultado != "Erro")
        //    {
        //        if (tipoCobranca != 1)
        //        {
        //            Solicitacao solicitacao = new Solicitacao
        //            {
        //                IdSolicitacao = Convert.ToInt32(resultado),
        //                IdEstudante = idEstudante,
        //                DataCriacao = DateTime.Now,
        //                IdFormaPagamento = tipoCobranca,
        //                IdCartao = idCartao,
        //                IdEndereco = idLocal,
        //                Finalizada = 0,
        //                Status = status
        //            };

        //            _context.Solicitacao.Add(solicitacao);

        //            await _context.SaveChangesAsync();

        //            return null;
        //        }
        //        else
        //        {
        //            DadosBoleto dadosBoleto = JsonConvert.DeserializeObject<DadosBoleto>(resultado);

        //            Solicitacao solicitacao = new Solicitacao
        //            {
        //                IdSolicitacao = dadosBoleto.IdSolicitacao,
        //                IdEstudante = idEstudante,
        //                DataCriacao = DateTime.Now,
        //                IdFormaPagamento = tipoCobranca,
        //                IdBoleto = dadosBoleto.IdBoleto,
        //                IdEndereco = idLocal,
        //                Finalizada = 0,
        //                Status = status
        //            };

        //            _context.Solicitacao.Add(solicitacao);

        //            await _context.SaveChangesAsync();

        //            return dadosBoleto;
        //        }
        //    }

        //    return null;
        //}

        //// ======= CLASSE PARA CADASTRAR UM NOVO CLIENTE VINDI
        //private async Task<string> CadastrarClienteVindiAsync(int idestudante)
        //{
        //    var dadosEstudante = _context.Estudantes.Where(x => x.IdEstudante == idestudante).ToList();

        //    var enderecos = _context.Enderecos.Where(x => x.IdEstudante == dadosEstudante[0].IdEstudante).ToList();

        //    var url = "https://app.vindi.com.br:443/api/v1/customers";

        //    var body = new PostClienteVindi.Cliente
        //    {
        //        name = dadosEstudante[0].Nome,
        //        email = dadosEstudante[0].Email,
        //        registry_code = dadosEstudante[0].Cpf,
        //        code = dadosEstudante[0].IdEstudante.ToString(),
        //        address = new PostClienteVindi.Address
        //        {
        //            street = enderecos[0].Logradouro,
        //            number = enderecos[0].Numero,
        //            zipcode = enderecos[0].Cep,
        //            neighborhood = enderecos[0].Bairro,
        //            city = enderecos[0].Cidade,
        //            state = enderecos[0].Estado,
        //            country = "BR"
        //        },
        //        phone = new PostClienteVindi.Phone
        //        {
        //            phone_type = "mobile",
        //            number = "55" + dadosEstudante[0].Celular
        //        }
        //    };

        //    string Json = JsonConvert.SerializeObject(body);

        //    string result = await VindiParametro.PostAsync(Json, url, "application/json");
        //    return result;
        //}

        //// ======= CLASSE PARA CADASTRAR UM NOVO CARTÃO
        //private async Task<string> CadastrarCartaoVindiAsync(int idVindi, PostCartaoVindi cartaoVindi)
        //{
        //    var url = "https://app.vindi.com.br:443/api/v1/payment_profiles";

        //    var body = new PostCartaoVindi
        //    {
        //        holder_name = cartaoVindi.holder_name,
        //        registry_code = cartaoVindi.registry_code,
        //        card_expiration = cartaoVindi.card_expiration,
        //        card_number = cartaoVindi.card_number,
        //        card_cvv = cartaoVindi.card_cvv,
        //        payment_method_code = "credit_card",
        //        payment_company_code = cartaoVindi.payment_company_code,
        //        customer_id = idVindi
        //    };

        //    string Json = JsonConvert.SerializeObject(body);

        //    string result = await VindiParametro.PostAsync(Json, url, "application/json");
        //    return result;
        //}

        //// ======= CLASSE PARA REALIZAR A COMPRA
        //private async Task<string> ComprarAsync(int idVindi, int idCartao, int idProduto, string metodo, double valor, string descricao)
        //{
        //    var url = "https://app.vindi.com.br:443/api/v1/bills";

        //    var body = new PostCompraVindi.Compra
        //    {
        //        customer_id = idVindi,
        //        installments = 1,
        //        payment_method_code = metodo,
        //        bill_items = new List<PostCompraVindi.BillItem>
        //                {
        //                    new PostCompraVindi.BillItem
        //                    {
        //                        product_id = idProduto,
        //                        amount = valor,
        //                        description = descricao
        //                    }
        //                },
        //        payment_profile = new PostCompraVindi.PaymentProfile
        //        {
        //            id = idCartao
        //        }
        //    };

        //    string Json = JsonConvert.SerializeObject(body);

        //    string result = await VindiParametro.PostAsync(Json, url, "application/json");

        //    return result;
        //}
    }
}