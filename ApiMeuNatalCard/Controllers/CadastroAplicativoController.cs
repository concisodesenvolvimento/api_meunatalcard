﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ApiMeuNatalCard;
using ApiMeuNatalCard.Model;

namespace ApiMeuNatalCard.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CadastroAplicativoController : ControllerBase
    {
        private readonly ApiContext _context;

        public CadastroAplicativoController(ApiContext context)
        {
            _context = context;
        }

        //// GET: api/CadastroAplicativo
        //[HttpGet]
        //public IEnumerable<CadastroAplicativo> GetCadastroAplicativo()
        //{
        //    return _context.CadastroAplicativo;
        //}

        //// GET: api/CadastroAplicativo/5
        //[HttpGet("{id}")]
        //public async Task<IActionResult> GetCadastroAplicativo([FromRoute] int id)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var cadastroAplicativo = await _context.CadastroAplicativo.FindAsync(id);

        //    if (cadastroAplicativo == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(cadastroAplicativo);
        //}

        //// PUT: api/CadastroAplicativo/5
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutCadastroAplicativo([FromRoute] int id, [FromBody] CadastroAplicativo cadastroAplicativo)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != cadastroAplicativo.IdCadastro)
        //    {
        //        return BadRequest();
        //    }

        //    _context.Entry(cadastroAplicativo).State = EntityState.Modified;

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!CadastroAplicativoExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return NoContent();
        //}

        //// POST: api/CadastroAplicativo
        //[HttpPost]
        //public async Task<IActionResult> PostCadastroAplicativo([FromBody] CadastroAplicativo cadastroAplicativo)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    _context.CadastroAplicativo.Add(cadastroAplicativo);
        //    await _context.SaveChangesAsync();

        //    return CreatedAtAction("GetCadastroAplicativo", new { id = cadastroAplicativo.IdCadastro }, cadastroAplicativo);
        //}

        //// DELETE: api/CadastroAplicativo/5
        //[HttpDelete("{id}")]
        //public async Task<IActionResult> DeleteCadastroAplicativo([FromRoute] int id)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var cadastroAplicativo = await _context.CadastroAplicativo.FindAsync(id);
        //    if (cadastroAplicativo == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.CadastroAplicativo.Remove(cadastroAplicativo);
        //    await _context.SaveChangesAsync();

        //    return Ok(cadastroAplicativo);
        //}

        //private bool CadastroAplicativoExists(int id)
        //{
        //    return _context.CadastroAplicativo.Any(e => e.IdCadastro == id);
        //}
    }
}