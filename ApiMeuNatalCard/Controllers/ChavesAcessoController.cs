﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ApiMeuNatalCard;
using ApiMeuNatalCard.Model;

namespace ApiMeuNatalCard.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChavesAcessoController : ControllerBase
    {
        private readonly ApiContext _context;

        public ChavesAcessoController(ApiContext context)
        {
            _context = context;
        }

        // GET: api/ChavesAcesso
        [HttpGet]
        public IEnumerable<ChavesAcesso> GetChavesAcesso()
        {
            return _context.ChavesAcesso;
        }

        // GET: api/ChavesAcesso/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetChavesAcesso([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var chavesAcesso = await _context.ChavesAcesso.FindAsync(id);

            if (chavesAcesso == null)
            {
                return NotFound();
            }

            return Ok(chavesAcesso);
        }

        // PUT: api/ChavesAcesso/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutChavesAcesso([FromRoute] int id, [FromBody] ChavesAcesso chavesAcesso)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != chavesAcesso.IdChave)
            {
                return BadRequest();
            }

            _context.Entry(chavesAcesso).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ChavesAcessoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ChavesAcesso
        [HttpPost]
        public async Task<IActionResult> PostChavesAcesso([FromBody] ChavesAcesso chavesAcesso)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.ChavesAcesso.Add(chavesAcesso);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetChavesAcesso", new { id = chavesAcesso.IdChave }, chavesAcesso);
        }

        // DELETE: api/ChavesAcesso/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteChavesAcesso([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var chavesAcesso = await _context.ChavesAcesso.FindAsync(id);
            if (chavesAcesso == null)
            {
                return NotFound();
            }

            _context.ChavesAcesso.Remove(chavesAcesso);
            await _context.SaveChangesAsync();

            return Ok(chavesAcesso);
        }

        private bool ChavesAcessoExists(int id)
        {
            return _context.ChavesAcesso.Any(e => e.IdChave == id);
        }
    }
}