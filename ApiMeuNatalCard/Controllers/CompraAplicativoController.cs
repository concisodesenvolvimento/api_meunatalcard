﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ApiMeuNatalCard.Model;
using Newtonsoft.Json;
using ApiMeuNatalCard.VindiPost;
using ApiMeuNatalCard.VindiGet;
using System;
using ApiMeuNatalCard.Classes.Http;
using ApiMeuNatalCard.Classes;

namespace ApiMeuNatalCard.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompraAplicativoController : ControllerBase
    {
        private readonly ApiContext _context;

        public CompraAplicativoController(ApiContext context)
        {
            _context = context;
        }

        // GET: api/CompraAplicativo
        [HttpGet]
        public IEnumerable<CompraAplicativo> GetCompraAplicativo()
        {
            return _context.CompraAplicativo;
        }

        // GET: api/CompraAplicativo/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCompraAplicativo([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var CompraAplicativo = await _context.CompraAplicativo.FindAsync(id);

            if (CompraAplicativo == null)
            {
                return NotFound();
            }

            return Ok(CompraAplicativo);
        }

        // GET: api/CompraAplicativo/5
        [HttpGet("BuscarBoletoRecarga/{idBoleto}")]
        public IActionResult GetCompraAplicativoBoletoRecarga([FromRoute] int idBoleto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var boleto = Boleto.BoletoAsync(idBoleto, false);

            if (boleto.Result.Item1 == null)
                return BadRequest();

            var resultado = new
            {
                Boleto = boleto.Result.Item1,
                Codigo = boleto.Result.Item2
            };

            return Ok(resultado);
        }

        // GET: api/CompraAplicativo/5
        [HttpGet("BuscarBoletoCie/{idBoleto}")]
        public IActionResult GetCompraAplicativoBoletoCie([FromRoute] int idBoleto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var boleto = Boleto.BoletoAsync(idBoleto, true);

            if (boleto.Result.Item1 == null)
                return BadRequest();

            var resultado = new
            {
                Boleto = boleto.Result.Item1,
                Codigo = boleto.Result.Item2
            };

            return Ok(resultado);
        }

        // PUT: api/CompraAplicativo/5
        [HttpPut("ReenviarSolicitacao/{idFatura}")]
        public async Task<IActionResult> PutCompraAplicativoReenviar([FromRoute] int idFatura)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            CompraAplicativo compraAplicativo = _context.CompraAplicativo.FirstOrDefault(c => c.IdFatura == idFatura && 
                                                                                        c.Servico == 2 && 
                                                                                        c.StatusCompra == 8);

            if(compraAplicativo == null)
                return NotFound();

            //string resultado = await MetodoHTTP.GetAsync("http://3.83.86.62:2907/solicitacao/reenviar/" + compraAplicativo.IdSolicitacao);
            string resultado = await MetodoHTTP.GetAsync("solicitacao/reenviar/" + compraAplicativo.IdSolicitacao);

            if (resultado == "NotFound" || resultado == "Erro")
                return BadRequest();

            compraAplicativo.StatusCompra = 2;
            
            _context.Entry(compraAplicativo).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return BadRequest();
            }

            return NoContent();
        }
        
        // PUT: api/CompraAplicativo/5
        [HttpPut("CancelarSolicitacao")]
        public async Task<IActionResult> PutCompraAplicativo([FromBody] CancelarCompra cancelarCompra)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            int idEstudante = _context.Estudantes.First(e => e.IdEstudanteIee == cancelarCompra.IdIee).IdEstudante;

            CompraAplicativo compraAplicativo = _context.CompraAplicativo.First(x => x.IdEstudante == idEstudante &&
                                                                                    x.IdFatura == cancelarCompra.IdFatura &&
                                                                                    x.Servico == 2);

            string url = "solicitacao/cancelar/" + compraAplicativo.IdSolicitacao;

            string resultadoCancelamento = await MetodoHTTP.GetAsync(url);

            if (resultadoCancelamento == "NotFound" || resultadoCancelamento == "Erro")
                return BadRequest("NãoCancelado");

            compraAplicativo.StatusCompra = 9;

            _context.Entry(compraAplicativo).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return BadRequest("NãoCancelado");
            }

            return NoContent();
        }

        // POST: api/CompraAplicativo
        [HttpPost]
        public async Task<IActionResult> PostCompraAplicativo([FromBody] DadosCompra dadosCompra)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Estudante estudante = _context.Estudantes.FirstOrDefault(x => x.IdEstudante == dadosCompra.IdEstudante);

            // ======= VERIFICA SE O ID DO ESTUDANTE EXISTE NA TABELA ESTUDANTE
            if (estudante != null)
            {
                CompraAplicativo compraAplicativo;

                if (dadosCompra.Servico == 1)
                {
                    if (dadosCompra.FormaPagamento == 16)
                    {
                        var CompraNoCartao = CompraNoCartaoAsync(dadosCompra, estudante.IdEstudanteIee, estudante.Cpf);

                        if (CompraNoCartao.Result.Item2 != "paid" && CompraNoCartao.Result.Item2 != "pending")
                            return BadRequest("NãoAutorizado");

                        compraAplicativo = CompraNoCartao.Result.Item1;

                        _context.CompraAplicativo.Add(compraAplicativo);

                        await _context.SaveChangesAsync();

                        var pagamento = new
                        {
                            compraAplicativo.IdSolicitacao,
                            compraAplicativo.IdFatura,
                            Transacao = CompraNoCartao.Result.Item2
                        };

                        return Ok(pagamento);
                    }
                    else
                    {
                        var resultado = await EnviarSolicitacaoAsync(dadosCompra, estudante.IdEstudanteIee, 1, estudante.Cpf);

                        if (resultado.Item1 == null)
                            return BadRequest("NãoAutorizado");

                        compraAplicativo = CompraAPP(dadosCompra.IdEstudante, resultado.Item1.IdBoleto, resultado.Item1.IdSolicitacao, 1, DateTime.Now, Convert.ToDecimal(dadosCompra.Valor), 1, dadosCompra.Servico, dadosCompra.idEndereco);

                        _context.CompraAplicativo.Add(compraAplicativo);

                        await _context.SaveChangesAsync();

                        var boleto = Boleto.BoletoAsync(resultado.Item1.IdBoleto, false);

                        var body = new
                        {
                            compraAplicativo.IdSolicitacao,
                            resultado.Item1.IdBoleto,
                            Boleto = boleto.Result.Item1,
                            Codigo = boleto.Result.Item2
                        };

                        return Ok(body);/*Ok(resultado.Item1.IdBoleto.ToString())*/;
                    }
                }
                else
                {
                    if (dadosCompra.FormaPagamento == 16)
                    {
                        var CompraNoCartao = CompraNoCartaoAsync(dadosCompra, estudante.IdEstudanteIee, estudante.Cpf);

                        if (CompraNoCartao.Result.Item2 != "paid" && CompraNoCartao.Result.Item2 != "pending")
                            return NotFound("Transação não realizada");

                        compraAplicativo = CompraNoCartao.Result.Item1;

                        _context.CompraAplicativo.Add(compraAplicativo);

                        await _context.SaveChangesAsync();

                        var pagamento = new
                        {
                            compraAplicativo.IdSolicitacao,
                            compraAplicativo.IdFatura,
                            Transacao = CompraNoCartao.Result.Item2
                        };

                        return Ok(pagamento);
                    }
                    else
                    {
                        var resultado = await EnviarSolicitacaoAsync(dadosCompra, estudante.IdEstudanteIee, 1, estudante.Cpf);

                        if (resultado.Item1 == null)
                            return BadRequest("NãoAutorizado");

                        decimal valor = dadosCompra.IdProduto == 8 ? 25 : Convert.ToDecimal(dadosCompra.Valor);

                        compraAplicativo = CompraAPP(dadosCompra.IdEstudante, resultado.Item1.IdBoleto, resultado.Item1.IdSolicitacao, 1, DateTime.Now, valor, 1, dadosCompra.Servico, dadosCompra.idEndereco);

                        _context.CompraAplicativo.Add(compraAplicativo);

                        await _context.SaveChangesAsync();

                        var boleto = Boleto.BoletoAsync(resultado.Item1.IdBoleto, true);

                        var body = new
                        {
                            compraAplicativo.IdSolicitacao,
                            resultado.Item1.IdBoleto,
                            Boleto = boleto.Result.Item1,
                            Codigo = boleto.Result.Item2
                        };

                        return Ok(body);/*Ok(resultado.Item1.IdBoleto.ToString())*/;
                    }
                }
            }
            else
                return NotFound();
        }

        // DELETE: api/CompraAplicativo/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCompraAplicativo([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var CompraAplicativo = await _context.CompraAplicativo.FindAsync(id);
            if (CompraAplicativo == null)
            {
                return NotFound();
            }

            _context.CompraAplicativo.Remove(CompraAplicativo);
            await _context.SaveChangesAsync();

            return Ok(CompraAplicativo);
        }

        private bool CompraAplicativoExists(int id)
        {
            return _context.CompraAplicativo.Any(e => e.IdCompra == id);
        }

        // -----------------------------------------------------------------------------------------------------------------------------
        // ======= COMPRA NO CARTÃO
        private async Task<Tuple<CompraAplicativo, string>> CompraNoCartaoAsync(DadosCompra dadosCompra, int idIee, string cpf)
        {
            // ======= VERIFICA SE EXISTE UM USUÁRIO CADASTRADO NA TABELA ESTUDANTEVINDI
            Vindi dados = _context.Vindi.FirstOrDefault(x => x.IdEstudante == dadosCompra.IdEstudante);

            // ============================================================================= //
            // ======================== CADASTRO DE USUÁRIO NA VINDI ======================= //

            int idVindiEstudante;
            int idCartaoEstudante;

            // ======= CASO A VARIÁVEL DADOS NÃO CONTENHA NENHUM DADOS, ENTÃO É REALIZADO O CADASTRO DO USUÁRIO
            if (dados == null)
            {
                string resultCliente = await CadastrarClienteVindiAsync(dadosCompra.IdEstudante);

                RespostaClienteVindi.Cliente cliente = JsonConvert.DeserializeObject<RespostaClienteVindi.Cliente>(resultCliente);

                idVindiEstudante = cliente.customer.id;

                Vindi vindi = new Vindi
                {
                    IdEstudante = dadosCompra.IdEstudante,
                    IdVindi = idVindiEstudante
                };

                _context.Vindi.Add(vindi);
                await _context.SaveChangesAsync();
            }
            else
                idVindiEstudante = dados.IdVindi;

            // ============================================================================= //
            // ======================== CADASTRO DE CARTÃO NA VINDI ======================== //

            // ======= SE O USUÁRIO ENVIAR OS DADOS DO CARTÃO, ENTÃO É REALIZADO O CADASTRO DO CARTÃO
            if (dadosCompra.Cartao != null)
            {
                string resultCartao = await CadastrarCartaoVindiAsync(idVindiEstudante, dadosCompra.Cartao);

                RespostaCartaoVindi.Cartao cartao = JsonConvert.DeserializeObject<RespostaCartaoVindi.Cartao>(resultCartao);

                idCartaoEstudante = cartao.payment_profile.id;
            }
            // ======= SE O USUÁRIO ENVIAR O ID DO CARTÃO, ENTÃO É ARMAZENADO NA VARIÁVEL ID CARTÃO
            else
                idCartaoEstudante = dadosCompra.IdCartao;

            // ============================================================================= //
            // ======= BUSCAR O PRODUTO CADASTRADO NA VINDI NO BANCO DE DADOS DO APP ======= //
            var produto = _context.Produto.First(x => x.IdProduto == dadosCompra.IdProduto);

            int idProduto = produto.IdProdutoVindi;

            string descricao = produto.Descricao;
            // ============================================================================= //

            double Valor = dadosCompra.IdProduto == 8 ? 25.00 : dadosCompra.Valor;

            // ======= ENVIA OS DADOS PARA REALIZAR A COMPRA (IDVINDI, IDCARTAO, IDPRODUTO, crédito, VALOR, DESCRIÇÃO)
            string resultCompra = await ComprarAsync(idVindiEstudante, idCartaoEstudante, idProduto, "credit_card", Valor, descricao);

            if (resultCompra == "")
                return new Tuple<CompraAplicativo, string>(null, "");

            RespostaCompraVindi.Fatura fatura = JsonConvert.DeserializeObject<RespostaCompraVindi.Fatura>(resultCompra);

            if (fatura != null)
            {
                // ======= STATUS DA COMPRA
                int statusCompra = fatura.bill.status == "paid" ? 2 : 1;
                //int idSolicitacao = -1;

                //if (statusCompra == 2)
                //{                
                var solicitacao = await EnviarSolicitacaoAsync(dadosCompra, idIee, 16, cpf);

                int idSolicitacao = solicitacao.Item2;

                // ======= ESTRUTURA PARA ARMAZENAR A COMPRA REALIZADA NO BD COMPRAAPLICATIVO
                CompraAplicativo compraAplicativo = CompraAPP(dadosCompra.IdEstudante, fatura.bill.id, idSolicitacao, 16, fatura.bill.created_at, Convert.ToDecimal(Valor), 2, dadosCompra.Servico, dadosCompra.idEndereco);

                return new Tuple<CompraAplicativo, string>(compraAplicativo, fatura.bill.status);
                //}
                //else
                //{
                //    // ======= CANCELAR COMPRA NA VINDI
                //    string url = "https://app.vindi.com.br:443/api/v1/bills/" + fatura.bill.id;

                //    // ======= ESTRUTURA PARA ARMAZENAR A COMPRA REALIZADA NO BD COMPRAAPLICATIVO
                //    CompraAplicativo compraAplicativo = CompraAPP(dadosCompra.IdEstudante, fatura.bill.id, idSolicitacao, 16, fatura.bill.created_at, Convert.ToDecimal(Valor), statusCompra, dadosCompra.Servico, dadosCompra.idEndereco);

                //    return new Tuple<CompraAplicativo, string>(compraAplicativo, fatura.bill.status);
                //}
            }
            else
                return new Tuple<CompraAplicativo, string>(null, "");
        }

        // ======= ENVIANDO DADOS PARA O BD SOLICITAÇÃO DE CARTEIRA
        private async Task<Tuple<DadosBoleto, int>> EnviarSolicitacaoAsync(DadosCompra dadosCompra, int idIee, int tipoCobranca, string cpf)
        {
            int local = dadosCompra.idEndereco != 0 ? 2 : 1;

            string Json = "";
            string url = "";

            if (dadosCompra.IdProduto == 8)
            {
                var body = new
                {
                    cpf,
                    local,
                    tipoCobranca
                };

                Json = JsonConvert.SerializeObject(body);
                //url = "http://3.83.86.62:2907/solicitacao";
                url = "solicitacao";
            }
            else
            {
                var body = new
                {
                    idestudante = idIee,
                    cpf,
                    dadosCompra.Valor,
                    tipoCobranca
                };

                Json = JsonConvert.SerializeObject(body);
                //url = "http://3.83.86.62:2907/recarga";
                url = "recarga";
            }

            string resultado = await MetodoHTTP.PostAsync(Json, url, "application/json");

            if (resultado != "NotFound" && resultado != "Erro")
            {
                if (tipoCobranca != 1)
                {
                    int idSolicitacao = JsonConvert.DeserializeObject<int>(resultado);
                    return new Tuple<DadosBoleto, int>(null, idSolicitacao);
                }
                else
                {
                    DadosBoleto dadosBoleto = JsonConvert.DeserializeObject<DadosBoleto>(resultado);

                    return new Tuple<DadosBoleto, int>(dadosBoleto, -1);
                }
            }

            return new Tuple<DadosBoleto, int>(null, -1); ;
        }

        // ======= ESTRUTURA PARA ARMAZENAR OS DADOS NO BD COMPRAAPLICATIVO
        private CompraAplicativo CompraAPP(int idEstudante, int idFatura, int idSolicitacao, int idFormaPagamento, DateTime dataCompra, decimal valor, int statusCompra, int servico, int idEndereco)
        {
            CompraAplicativo compra = new CompraAplicativo
            {
                IdEstudante = idEstudante,
                IdFatura = idFatura,
                IdSolicitacao = idSolicitacao,
                IdFormaPagamento = idFormaPagamento,
                DataCompra = dataCompra,
                Valor = valor,
                StatusCompra = statusCompra,
                Servico = servico,
                IdEndereco = idEndereco
            };

            return compra;
        }

        // ======= CLASSE PARA CADASTRAR UM NOVO CLIENTE VINDI
        private async Task<string> CadastrarClienteVindiAsync(int idestudante)
        {
            var dadosEstudante = _context.Estudantes.Where(x => x.IdEstudante == idestudante).ToList();

            var enderecos = _context.Enderecos.Where(x => x.IdEstudante == dadosEstudante[0].IdEstudante).ToList();

            var url = "https://app.vindi.com.br:443/api/v1/customers";

            var body = new PostClienteVindi.Cliente
            {
                name = dadosEstudante[0].Nome,
                email = dadosEstudante[0].Email,
                registry_code = dadosEstudante[0].Cpf,
                code = dadosEstudante[0].IdEstudante.ToString(),
                address = new PostClienteVindi.Address
                {
                    street = enderecos[0].Logradouro,
                    number = enderecos[0].Numero,
                    zipcode = enderecos[0].Cep,
                    neighborhood = enderecos[0].Bairro,
                    city = enderecos[0].Cidade,
                    state = enderecos[0].Estado,
                    country = "BR"
                },
                phone = new PostClienteVindi.Phone
                {
                    phone_type = "mobile",
                    number = "55" + dadosEstudante[0].Celular
                }
            };

            string Json = JsonConvert.SerializeObject(body);

            string result = await VindiParametro.PostAsync(Json, url, "application/json");
            return result;
        }

        // ======= CLASSE PARA CADASTRAR UM NOVO CARTÃO
        private async Task<string> CadastrarCartaoVindiAsync(int idVindi, PostCartaoVindi cartaoVindi)
        {
            var url = "https://app.vindi.com.br:443/api/v1/payment_profiles";

            var body = new PostCartaoVindi
            {
                holder_name = cartaoVindi.holder_name,
                registry_code = cartaoVindi.registry_code,
                card_expiration = cartaoVindi.card_expiration,
                card_number = cartaoVindi.card_number,
                card_cvv = cartaoVindi.card_cvv,
                payment_method_code = "credit_card",
                payment_company_code = cartaoVindi.payment_company_code,
                customer_id = idVindi
            };

            string Json = JsonConvert.SerializeObject(body);

            string result = await VindiParametro.PostAsync(Json, url, "application/json");
            return result;
        }

        // ======= CLASSE PARA REALIZAR A COMPRA
        private async Task<string> ComprarAsync(int idVindi, int idCartao, int idProduto, string metodo, double valor, string descricao)
        {
            var url = "https://app.vindi.com.br:443/api/v1/bills";

            var body = new PostCompraVindi.Compra
            {
                customer_id = idVindi,
                installments = 1,
                payment_method_code = metodo,
                bill_items = new List<PostCompraVindi.BillItem>
                        {
                            new PostCompraVindi.BillItem
                            {
                                product_id = idProduto,
                                amount = valor,
                                description = descricao
                            }
                        },
                payment_profile = new PostCompraVindi.PaymentProfile
                {
                    id = idCartao
                }
            };

            string Json = JsonConvert.SerializeObject(body);

            string result = await VindiParametro.PostAsync(Json, url, "application/json");

            return result;
        }
        
    }

    public class CancelarCompra
    {
        public int IdIee { get; set; }
        public int IdFatura { get; set; }
    }
}