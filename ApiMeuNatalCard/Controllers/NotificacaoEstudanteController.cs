﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ApiMeuNatalCard.Model;

namespace ApiMeuNatalCard.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotificacaoEstudanteController : ControllerBase
    {
        private readonly ApiContext _context;

        public NotificacaoEstudanteController(ApiContext context)
        {
            _context = context;
        }

        //// GET: api/NotificacaoEstudante
        //[HttpGet]
        //public IEnumerable<NotificacaoEstudante> GetNotificacaoEstudante()
        //{
        //    return _context.NotificacaoEstudante;
        //}

        // POST: api/NotificacaoEstudante/Notificar
        //[HttpPost("Notificar")]
        //public async Task<IActionResult> PostNotificacaoEstudante()
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    ChavesAcesso chavesAcesso = _context.ChavesAcesso.First();

        //    List<NotificacaoEstudante> notificacaos = _context.NotificacaoEstudante.Where(n => n.Enviado == 0 && n.Lido == 0).ToList();

        //    for (int n = 0; n < notificacaos.Count; n++)
        //    {
        //        List<AcessoAplicativo> acesso = _context.AcessoAplicativo.Where(a => a.IdEstudante == notificacaos[n].IdEstudante &&
        //                                                               a.Ativo == 1 &&
        //                                                               a.Falha == 0).ToList();

        //        int totalFalha = 0; // número total de falhas

        //        for (int a = 0; a < acesso.Count; a++)
        //        {
        //            int falha = EnviarNotificacao(chavesAcesso.FCM_ChaveHServidor, chavesAcesso.FCM_Remetente, acesso[a].Token, notificacaos[n].Titulo, notificacaos[n].Mensagem);
                    
        //            if (falha == 1)
        //            {
        //                totalFalha = totalFalha + falha;

        //                acesso[a].Falha = 1;

        //                _context.Entry(acesso[a]).State = EntityState.Modified;
        //            }

        //            if (falha != 1 || totalFalha == acesso.Count)
        //            {
        //                notificacaos[n].Enviado = 1;
        //                notificacaos[n].DataNotificacao = DateTime.Now;

        //                _context.Entry(notificacaos[n]).State = EntityState.Modified;
        //            }

        //            await _context.SaveChangesAsync();
        //        }
        //    }

        //    return Ok();
        //}

        // PUT: api/NotificacaoEstudante/5
        [HttpPut("Atualizar/{idestudante}")]
        public async Task<IActionResult> PutNotificacaoEstudante([FromRoute] int idestudante)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            List<NotificacaoEstudante> notificacaoEstudante = _context.NotificacaoEstudante.Where(n => n.IdEstudante == idestudante && n.Lido == 0).ToList();

            for (int i = 0; i < notificacaoEstudante.Count; i++)
            {
                notificacaoEstudante[i].Lido = 1;
                notificacaoEstudante[i].DataLeitura = DateTime.Now;

                _context.Entry(notificacaoEstudante[i]).State = EntityState.Modified;
            }

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }

            return NoContent();
        }
        
        //public int EnviarNotificacao(string chaveHServidor, string remetente, string token, string titutlo, string message)
        //{
        //    try
        //    {
        //        System.Net.WebRequest tRequest = System.Net.WebRequest.Create("https://fcm.googleapis.com/fcm/send");
        //        tRequest.Method = "post";
        //        tRequest.ContentType = "application/json";

        //        var payload = new
        //        {
        //            to = token,
        //            priority = "high",
        //            content_available = true,
        //            notification = new
        //            {
        //                body = message,
        //                title = titutlo,
        //                icon = "icone_meunatalcard",
        //                color = "#2C7E50",
        //                sound = "default"
        //            },
        //            data = new
        //            {
        //                body = message,
        //                title = titutlo,
        //                icon = "icone_meunatalcard",
        //                color = "#2C7E50",
        //                sound = "default"
        //            }
        //        };

        //        var json = Newtonsoft.Json.JsonConvert.SerializeObject(payload).ToString();
        //        Byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(json);

        //        tRequest.Headers.Add(string.Format("Authorization: key={0}", chaveHServidor));
        //        tRequest.Headers.Add(string.Format("Sender: id={0}", remetente));
        //        tRequest.ContentLength = byteArray.Length;

        //        using (System.IO.Stream dataStream = tRequest.GetRequestStream())
        //        {
        //            dataStream.Write(byteArray, 0, byteArray.Length);

        //            using (System.Net.WebResponse tResponse = tRequest.GetResponse())
        //            {
        //                using (System.IO.Stream dataStreamResponse = tResponse.GetResponseStream())
        //                {
        //                    using (System.IO.StreamReader tReader = new System.IO.StreamReader(dataStreamResponse))
        //                    {
        //                        String sResponseFromServer = tReader.ReadToEnd();

        //                        ResultadoNotificacao.RootObject resultadoNotificacao = Newtonsoft.Json.JsonConvert.DeserializeObject<ResultadoNotificacao.RootObject>(sResponseFromServer);

        //                        return resultadoNotificacao.Failure;
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch
        //    {
        //        return -1;
        //    }
        //}
    }
}