﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ApiMeuNatalCard.Model;
using System.Security.Cryptography;
using System.Text;
using ApiMeuNatalCard.Classes.Http;
using ApiMeuNatalCard.Classes.ApiIee;
using Newtonsoft.Json;
using ApiMeuNatalCard.Classes;

namespace ApiMeuNatalCard.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EstudantesController : ControllerBase
    {
        private readonly ApiContext _context;

        public EstudantesController(ApiContext context)
        {
            _context = context;
        }

        // GET: api/Estudantes
        [HttpGet]
        public IEnumerable<Estudante> GetEstudante()
        {
            return _context.Estudantes;
        }

        // GET: api/Estudantes/recuperarsenha/5
        [HttpGet("RecuperarSenha/{cpf}")]
        public IActionResult GetHistorico([FromRoute] string cpf)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Estudante estudante = _context.Estudantes.FirstOrDefault(e => e.Cpf == cpf);

            if (estudante == null)
                return BadRequest();

            string email = estudante.Email;

            int indice1 = email.IndexOf("@");

            string emailInicio = email.Substring(0, 3);

            string emailFinal = email.Substring(indice1);

            string emailMascara = emailInicio + "*******" + emailFinal;

            return Ok(emailMascara.ToLower());
        }

        // GET: api/Estudantes/historicorecarga/5
        [HttpGet("HistoricoRecarga/{idEstudante}")]
        public async Task<IActionResult> GetHistoricoAsync([FromRoute] int idEstudante)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            string cpf = _context.Estudantes.FirstOrDefault(e => e.IdEstudante == idEstudante).Cpf;

            if (cpf == null)
                return BadRequest("Erro");

            //string url = "http://3.83.86.62:2907/estudante/recarga/" + cpf;
            string url = "estudante/recarga/" + cpf;

            string resposta = await MetodoHTTP.GetAsync(url);

            if (resposta == "Erro" || resposta == "NotFound")
                return BadRequest("Erro");

            List<DadosHistorico> dadosHistoricos = new List<DadosHistorico>();
            dadosHistoricos = JsonConvert.DeserializeObject<List<DadosHistorico>>(resposta);

            for (int i = 0; i < dadosHistoricos.Count; i++)
            {
                dadosHistoricos[i].Status = (int)Status(dadosHistoricos[i].Status);

                if (dadosHistoricos[i].Status == 1)
                {
                    try
                    {
                        int fatura = _context.CompraAplicativo.FirstOrDefault(c => c.IdEstudante == idEstudante &&
                                                                                        c.StatusCompra == 1 &&
                                                                                        c.Servico == 1 &&
                                                                                        //c.IdFormaPagamento == 1 &&
                                                                                        c.IdSolicitacao == dadosHistoricos[i].IdRecargaSolicitacao).IdFatura;

                        dadosHistoricos[i].IdBoleto = fatura;
                    }
                    catch
                    {
                        dadosHistoricos[i].IdBoleto = -1;
                    }
                }
                else
                    dadosHistoricos[i].IdBoleto = -1;
            }

            return Ok(dadosHistoricos);
        }

        private RecargaStatus Status(int? status)
        {
            if (status == 1)
                return RecargaStatus.AguardandoPagamento;
            if (status == 2 || status == 4)
                return RecargaStatus.Pago;
            else if (status == 3)
                return RecargaStatus.Cancelado;
            else if (status == 5)
                return RecargaStatus.CreditoLiberado;
            else if (status == 6)
                return RecargaStatus.CreditoNaoLiberado;
            else
                return RecargaStatus.Nulo;
        }

        // GET: api/Estudantes/recuperarsenha/5
        [HttpGet("Solicitacao/{idSolicitacao}")]
        public IActionResult GetSolicitacao([FromRoute] int idSolicitacao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            CompraAplicativo compraAplicativo = _context.CompraAplicativo.FirstOrDefault(c => c.IdSolicitacao == idSolicitacao &&
                                                                                         c.Servico == 2);

            if (compraAplicativo == null)
                return BadRequest();

            string mensagemNotificacao = "";
            int reenviar = 0;

            if (compraAplicativo.StatusCompra == 8)
            {
                mensagemNotificacao = _context.NotificacaoEstudante.LastOrDefault(n => n.IdEstudante == compraAplicativo.IdEstudante).Mensagem;

                string[] palavras = mensagemNotificacao.Split(' ');

                if ((palavras.Contains("FOTO") || palavras.Contains("foto")) &&
                    (palavras.Contains("escola") || palavras.Contains("ESCOLA")))
                    reenviar = 3;
                else if (palavras.Contains("FOTO") || palavras.Contains("foto"))
                    reenviar = 1;
                else
                    reenviar = 2;
            }

            var retorno = new
            {
                compraAplicativo.StatusCompra,
                compraAplicativo.IdEndereco,
                compraAplicativo.IdFatura,
                compraAplicativo.IdFormaPagamento,
                mensagemNotificacao,
                reenviar
            };

            return Ok(retorno);
        }

        // PUT: api/Estudantes/endereco/5
        [HttpPut("Endereco/{idEstudante}")]
        public async Task<IActionResult> PutEstudante([FromRoute] int idEstudante, [FromBody] AtualizarEndereco atualizarEndereco)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                Endereco enderecoAntigo = _context.Enderecos.First(e => e.IdEstudante == idEstudante && e.Ativo == 1);

                enderecoAntigo.Ativo = 0;

                _context.Entry(enderecoAntigo).State = EntityState.Modified;

                await _context.SaveChangesAsync();

                Endereco enderecoNovo = new Endereco
                {
                    IdEstudante = idEstudante,
                    Logradouro = atualizarEndereco.Logradouro,
                    Numero = atualizarEndereco.Numero,
                    Cep = atualizarEndereco.Cep,
                    Bairro = atualizarEndereco.Bairro,
                    Cidade = atualizarEndereco.Cidade,
                    Estado = atualizarEndereco.Estado,
                    Principal = atualizarEndereco.Principal,
                    Ativo = atualizarEndereco.Ativo,
                };

                _context.Enderecos.Add(enderecoNovo);

                await _context.SaveChangesAsync();

                Estudante estudante = _context.Estudantes.First(e => e.IdEstudante == idEstudante);

                estudante.Celular = atualizarEndereco.Celular;

                _context.Entry(estudante).State = EntityState.Modified;

                await _context.SaveChangesAsync();

                int idEndereco = _context.Enderecos.First(e => e.IdEstudante == idEstudante && e.Ativo == 1).IdEndereco;

                return Ok(idEndereco);
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }
        }

        // PUT: api/Estudantes/endereco/5
        [HttpPut("AlterarEmail/{idEstudante}")]
        public async Task<IActionResult> PutEstudanteEmail([FromRoute] int idEstudante, [FromBody] AlterarEmail alterarEmail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                Estudante estudante = _context.Estudantes.FirstOrDefault(e => e.IdEstudante == idEstudante);

                if (estudante == null)
                    return NotFound();

                if (estudante.Email.ToUpper() != alterarEmail.EmailAntigo.ToUpper())
                    return BadRequest("Diferente");

                estudante.Email = alterarEmail.EmailNovo.ToUpper();

                _context.Entry(estudante).State = EntityState.Modified;

                await _context.SaveChangesAsync();

                return Ok();
            }
            catch (DbUpdateConcurrencyException)
            {
                return BadRequest();
            }
        }

        // PUT: api/Estudantes/endereco/5
        [HttpPut("AlterarSenha/{idEstudante}")]
        public async Task<IActionResult> PutEstudanteSenha([FromRoute] int idEstudante, [FromBody] AlterarSenha alterarSenha)
        {
            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}

            try
            {
                Estudante estudante = _context.Estudantes.FirstOrDefault(e => e.IdEstudante == idEstudante);

                if (estudante == null)
                    return NotFound();

                string senhaAntigaHash = Criptografar.Sha256_hash(alterarSenha.SenhaAntiga);

                if (estudante.SenhaHash != senhaAntigaHash)
                {
                    return BadRequest("Diferente");
                }
                else
                {

                    return Ok(senhaAntigaHash);
                }

                string senhaNovaHash = Criptografar.Sha256_hash(alterarSenha.SenhaNova);

                estudante.SenhaHash = senhaNovaHash;

                _context.Entry(estudante).State = EntityState.Modified;

                await _context.SaveChangesAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // POST: api/Estudantes/verificar
        [HttpPost("verificar")]
        public async Task<IActionResult> GetEstudanteAsync([FromBody] VerificarEstudante verificarEstudante)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var estudante = _context.Estudantes.Where(x => x.Cpf == verificarEstudante.Cpf).FirstOrDefault();

            if (estudante != null)
                return NoContent();
            else
            {
                DadosApiIee.ApiIee dados = new DadosApiIee.ApiIee();

                //string dadosApiIee = await MetodoHTTP.GetAsync("http://3.83.86.62:2907/estudante/" + verificarEstudante.Cpf);
                string dadosApiIee = await MetodoHTTP.GetAsync("estudante/" + verificarEstudante.Cpf);

                if (dadosApiIee == "NotFound")
                {
                    return NotFound("NotFound");
                }
                else if (dadosApiIee == "Erro")
                {
                    return BadRequest("Erro");
                }
                else
                {
                    dados = JsonConvert.DeserializeObject<DadosApiIee.ApiIee>(dadosApiIee);

                    if (verificarEstudante.DataNascimento.ToShortDateString() != dados.DataNascimento.ToShortDateString())
                        return BadRequest("Erro");
                }

                return Ok(dados);
            }
        }

        // POST: api/Estudantes
        [HttpPost]
        public async Task<IActionResult> PostEstudante([FromBody] RegistroEstudante registro)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // criptografar
            registro.Estudante.SenhaHash = Criptografar.Sha256_hash(registro.Estudante.SenhaHash);

            _context.Estudantes.Add(registro.Estudante);

            await _context.SaveChangesAsync();

            int idEstudante = _context.Estudantes.First(e => e.Cpf == registro.Estudante.Cpf).IdEstudante;

            ConfigEstudante configEstudante = new ConfigEstudante
            {
                IdEstudante = idEstudante,
                IdConfig = 2
            };

            _context.ConfigEstudante.Add(configEstudante);

            await _context.SaveChangesAsync();

            Saldos saldos = new Saldos
            {
                IdEstudante = idEstudante,
                Saldo = registro.Saldo,
                DataAtualizacao = DateTime.Now
            };

            _context.Saldos.Add(saldos);

            await _context.SaveChangesAsync();

            CadastroAplicativo cadastroAplicativo = new CadastroAplicativo
            {
                IdEstudante = idEstudante,
                DataCriacao = DateTime.Now,
                TermoAceito = 'S',
                DataTermoAceito = DateTime.Now,
                EnderecoIp = ""
            };

            _context.CadastroAplicativo.Add(cadastroAplicativo);

            await _context.SaveChangesAsync();

            return Ok();
        }

        // POST: api/Estudantes/email/5
        [HttpGet("Email/{cpfEstudante}")]
        public async Task<IActionResult> GetEstudanteEmailAsync([FromRoute] string cpfEstudante)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Estudante estudante = _context.Estudantes.FirstOrDefault(e => e.Cpf == cpfEstudante);

            if (estudante == null)
                return BadRequest();
            
            try
            {
                string senhaNova = SenhaAleatoria.AlfanumericoAleatorio(6);

                string senhaHash = Criptografar.Sha256_hash(senhaNova);
                senhaHash = Criptografar.Sha256_hash(senhaHash);

                estudante.SenhaHash = senhaHash;

                _context.Entry(estudante).State = EntityState.Modified;

                await _context.SaveChangesAsync();

                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                System.Net.Mail.SmtpClient SmtpServer = new System.Net.Mail.SmtpClient("email-smtp.us-east-1.amazonaws.com");

                mail.From = new System.Net.Mail.MailAddress("meunatalcard@natalcard.com.br");
                mail.To.Add(estudante.Email.ToLower());
                mail.Subject = "Meu NatalCard - Recuperação de senha.";
                mail.Body = "<p>Olá, " + estudante.Nome + ",</p>" +
                    "<p></p>" +
                    "<p>Você solicitou uma nova senha de acesso ao aplicativo Meu NatalCard.</p>" +
                    "<p></p>" +
                    "<p></p>" +
                    "<p>Copie e cole o código abaixo para acessar o aplicativo e sem seguida altere a senha no painel Configurações do aplicativo.</p>" +
                    "<p></p>" +
                    "<p>Seu senha é: " + senhaNova + " </p>" +
                    "<p></p>" +
                    "<p>NatalCard</p>" +
                    "<p>Suporte Ti</p>" +
                    "<p>Não responder este email.</p>" +
                    "<p><img src=\"http://nbe.concisoti.com.br/novo/img/natalcard.png\"/>";

                mail.IsBodyHtml = true;

                SmtpServer.Port = 587;
                SmtpServer.Host = "email-smtp.us-east-1.amazonaws.com";
                SmtpServer.EnableSsl = true;
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.Credentials = new System.Net.NetworkCredential("AKIA27WMBHVUP6BSTXZ5", "BCY9IA8xSvrcu5AeG6zwQ+WeSMAtEbpYKYj23xp+ipx6");

                SmtpServer.Send(mail);

                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }

        private bool EstudanteExists(int id)
        {
            return _context.Estudantes.Any(e => e.IdEstudante == id);
        }
    }
}