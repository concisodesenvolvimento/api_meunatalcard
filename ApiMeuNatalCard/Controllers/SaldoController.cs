﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ApiMeuNatalCard;
using ApiMeuNatalCard.Model;

namespace ApiMeuNatalCard.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SaldoController : ControllerBase
    {
        private readonly ApiContext _context;

        public SaldoController(ApiContext context)
        {
            _context = context;
        }

        // GET: api/Saldo
        [HttpGet]
        public IEnumerable<Saldos> GetSaldos()
        {
            return _context.Saldos;
        }

        // GET: api/Saldo/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSaldos([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var saldos = await _context.Saldos.FindAsync(id);

            if (saldos == null)
            {
                return NotFound();
            }

            return Ok(saldos);
        }

        // PUT: api/Saldo/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSaldos([FromRoute] int id, [FromBody] Saldos saldos)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != saldos.IdEstudante)
            {
                return BadRequest();
            }

            _context.Entry(saldos).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SaldosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Saldo
        [HttpPost]
        public async Task<IActionResult> PostSaldos([FromBody] Saldos saldos)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Saldos.Add(saldos);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSaldos", new { id = saldos.IdEstudante }, saldos);
        }

        // DELETE: api/Saldo/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSaldos([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var saldos = await _context.Saldos.FindAsync(id);
            if (saldos == null)
            {
                return NotFound();
            }

            _context.Saldos.Remove(saldos);
            await _context.SaveChangesAsync();

            return Ok(saldos);
        }

        private bool SaldosExists(int id)
        {
            return _context.Saldos.Any(e => e.IdEstudante == id);
        }
    }
}