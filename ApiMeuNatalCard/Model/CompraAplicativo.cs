﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiMeuNatalCard.Model
{
    [Table("CompraAplicativo")]
    public class CompraAplicativo
    {
        [Key]
        public int IdCompra { get; set; }

        public int IdFatura { get; set; }
        public int IdEstudante { get; set; }
        public int IdSolicitacao { get; set; }
        public int IdFormaPagamento { get; set; }
        public DateTime DataCompra { get; set; }
        public decimal Valor { get; set; }
        public int StatusCompra { get; set; }
        public int Servico { get; set; }
        public int? IdEndereco { get; set; }
    }
}
