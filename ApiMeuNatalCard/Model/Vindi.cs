﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiMeuNatalCard.Model
{
    [Table("EstudanteVindi")]
    public class Vindi
    {
        [Key]
        public int IdVindi { get; set; }
        public int IdEstudante { get; set; }
    }
}
