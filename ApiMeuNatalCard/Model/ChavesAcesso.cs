﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiMeuNatalCard.Model
{
    [Table("ChavesAcesso")]
    public class ChavesAcesso
    {
        [Key]
        public int IdChave { get; set; }

        public string VindiChave { get; set; }
        public string FCM_ChaveHServidor { get; set; }
        public string FCM_Remetente { get; set; }
    }
}
