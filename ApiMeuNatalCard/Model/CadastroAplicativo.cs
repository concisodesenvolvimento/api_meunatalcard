﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiMeuNatalCard.Model
{
    [Table("CadastroAplicativo")]
    public class CadastroAplicativo
    {
        [Key]
        public int IdCadastro { get; set; }

        public int IdEstudante { get; set; }
        public DateTime DataCriacao { get; set; }
        public char TermoAceito { get; set; }
        public DateTime DataTermoAceito { get; set; }
        public string EnderecoIp { get; set; }
    }
}
