﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiMeuNatalCard.Model
{
    [Table("AcessoAplicativo")]
    public class AcessoAplicativo
    {
        [Key]
        public int IdAcesso { get; set; }

        public int IdEstudante { get; set; }
        public string Token { get; set; }
        public DateTime Data_Logon { get; set; }
        public DateTime? Data_Logof { get; set; }
        public int Ativo { get; set; }
        public int? Falha { get; set; }
    }
}
