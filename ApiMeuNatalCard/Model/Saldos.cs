﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiMeuNatalCard.Model
{
    [Table("Saldos")]
    public class Saldos
    {
        [Key]
        public int IdEstudante { get; set; }

        public decimal Saldo { get; set; }
        public DateTime DataAtualizacao { get; set; }
    }
}
