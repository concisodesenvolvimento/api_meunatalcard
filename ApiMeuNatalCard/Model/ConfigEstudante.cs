﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiMeuNatalCard.Model
{
    [Table("ConfigEstudante")]
    public class ConfigEstudante
    {
        [Key]
        public int IdEstudante { get; set; }

        public int IdConfig { get; set; }
    }
}
