﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiMeuNatalCard.Model
{
    [Table("NotificacaoEstudante")]
    public class NotificacaoEstudante
    {
        [Key]
        public int? IdNotificacao { get; set; }
        
        public int IdEstudante { get; set; }
        public string Titulo { get; set; }
        public string Mensagem { get; set; }
        public DateTime? DataNotificacao { get; set; }
        public int Lido { get; set; }
        public DateTime? DataLeitura { get; set; }
        public int? Enviado { get; set; }
        public DateTime? DataCriacao { get; set; }
        public int? Tipo { get; set; }
    }
}
