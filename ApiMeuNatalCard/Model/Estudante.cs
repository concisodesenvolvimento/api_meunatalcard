﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ApiMeuNatalCard.Model
{
    [Table("Estudante")]
    public class Estudante
    {
        [Key]
        public int IdEstudante { get; set; }

        public string Cpf { get; set; }
        public string Rg { get; set; }
        public string Matricula { get; set; }
        public string Curso { get; set; }
        public string Nivel { get; set; }
        public string Nome { get; set; }
        public DateTime DataNascimento { get; set; }
        public string Celular { get; set; }
        public string Email { get; set; }
        public string SenhaHash { get; set; }
        public int IdEstudanteIee { get; set; }
        public string Instituicao { get; set; }
        public virtual Endereco Endereco { get; set; }
        public virtual List<NotificacaoEstudante> NotificacaoEstudante { get; set; }
    }
}
